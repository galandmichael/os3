#include "param.h"
#include "types.h"
#include "stat.h"
#include "user.h"
#include "fs.h"
#include "fcntl.h"
#include "syscall.h"
#include "traps.h"
#include "memlayout.h"

#define PGSIZE 4096
#define NUMBER_OF_PAGES_TO_TEST 16

char* memory[NUMBER_OF_PAGES_TO_TEST]; //Represents the memoty
int s = 1;

volatile int
main(int argc, char *argv[])
{
	int pid, i, j;
	int childTestPassed = 1;

	for (i = 0; i < NUMBER_OF_PAGES_TO_TEST ; i++) {
		memory[i] = sbrk(PGSIZE);
		printf(1, "allocated page #%d at address: %x\n", i, memory[i]);
	}

	for ( i = 0; i < NUMBER_OF_PAGES_TO_TEST; i++) {
		for ( j = 0; j < PGSIZE; j++) {
			memory[i][j] = 1;
		}
	}

	printf(1,"Put all memory values to be 1\n\n");

	if (!s) {
		printf(1,"Finished Successfuly!!!\n");
		exit();
		return 0;
	} 

	pid = fork();

	if (pid) {
		printf(1,"Parent continued after fork\n");
		wait();

		printf(1,"Parent woke up\n");
		printf(1,"Finished Successfuly!!!\n");
	} else {

		printf(1,"Child checking that memory was coppied correctly\n");
		for ( i = 0; i < NUMBER_OF_PAGES_TO_TEST; i++) {
			for ( j = 0; j < PGSIZE; j++) {
				if (memory[i][j] != 1) {
					childTestPassed = 0;
				}
			}
		}
		if (childTestPassed) {
			printf(1,"Child test passed!\n\n");
		} else {
			printf(1,"Child test failed!\n\n");
		}

		printf(1,"Child continued after fork. Going to sleep\n");
		sleep(10);
		printf(1,"Child woke up\n");
	}

	exit();
	return 0;
}